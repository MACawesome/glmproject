* Part 1
** Question 3

Number of knots should be less than the number of covariate values

P-splines can be used to do the smoothing.

* Part 2

* Other stuff
** Example exam projects
I don't distinguish the papers

18 and 19 pages, a lot of figures and tables. 
*** Part 1
Use of a correlation matrix figure in the description. Check of
differences in outcome with overlapped histograms. 

In part 1, the GLM equation is written down as the distribution of the
outcome and the linear predictor. Poisson regression with log link
function is presented.

Only the linear predictor is provided for the poisson regression. 

In both projects, use score and likelihood ratio tests for model
selection. Comparison with stepwise model selection. 

Outlier detection *and* deletion. Done with Pearson residuals and
Cook's distance. 

Global goodness-of-fit is checked with Pearson and Deviance
statistics.

The range of predictions is checked. Interesting to see if the range
of observed outcomes is reproduced by the fitted model. Zero-inflation?

Predicted counts can be presented against a single varying variable
(or maybe we could show variation across multiple categories for a
single covariate). 

The Poisson, negative-binomial, quasi-Poisson and quasi-Poisson with
sandwich estimator (why bother?) are compared. Observed outcome counts
are compared to the density curves of predicted counts. 

*** Part 2
All the smoothing techniques are put into the same sections. Short
explanation of each techniques. Big emphasis on graphs. 

Smoothing curves with the same degree and knots are compared on a same
graph. Few details on the P-splines, they are treated separately. 

No interactions in the additive model (makes sense, difficult to
implement). P-splines are used as basis functions. Adjusted $R^2$, GCV
(leave-one out cv I think) and AIC are used as goodness-of-fit
measures. 

Check of additive model's assumptions (residuals, normality etc., this
is possible with a builtin function of mgcv). To correct for
non-normality, log transformation of response. Doesn't seem mandated
by the histogram of residuals which doesn't look bad at all. The team
interpret lower AIC and GCV of the *log-transformed* model as
indicating a better model, but they don't scale them back! Big
mistake. 

Log-transformation is used in each approach. Not really warranted it
seems.

Model comparisons with the GAM and added covariates. 

GCV is not a form of regularisation, it's use to get the right
hyper-parameter for regularisation.

Correlation doesn't imply interaction of one effect with another. 

For model selection: choose which variable can be treated as linear,
remove non-significant variables. 

Possibility of non-linearity by fitting a gam to the binary outcome.

