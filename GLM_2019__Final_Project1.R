###Project 1: Bike sharing systems###

#import dataset
bike_all=read_excel("/Users/halabanz/Desktop/GLM1/bike_sharing.xlsx")

#Extract the following variables from the data set: 
#weathersit, workingday, temp, hum, windspeed, season and cnt
bike=bike_all[,c(15,7,8,9,11,12,3)]
str(bike)
summary(bike)
range(bike$cnt)

#change numeric to factor type
bike$workingday=as.factor(bike$workingday)
bike$weathersit=as.factor(bike$weathersit)
bike$season=as.factor(bike$season)

##Descriptive Statitics (weathersit, workingday, temp, hum, windspeed, season and cnt)##

#Categorical variables (weathersit, workingday,season)
p1=ggplot(data=bike,aes(x=weathersit,y=cnt,color=weathersit))+geom_boxplot();p1
p2=ggplot(data=bike,aes(x=workingday,y=cnt,color=workingday))+geom_boxplot();p2
p3=ggplot(data=bike,aes(x=season,y=cnt,color=season))+geom_boxplot();p3

grid.arrange(p1, p2, p3,nrow=2, ncol=2)

#count occurrences

plot1=ggplot(data=bike,aes(x=weathersit,fill=cnt))+geom_bar(fill='light blue')+
  geom_text(stat='count', aes(label=..count..), vjust=0.1);plot1
plot2=ggplot(data=bike,aes(x=workingday,fill=cnt))+geom_bar(fill='light blue')+
  geom_text(stat='count', aes(label=..count..), vjust=0.1);plot2
plot3=ggplot(data=bike,aes(x=season,fill=cnt))+geom_bar(fill='light blue')+
  geom_text(stat='count', aes(label=..count..), vjust=0.1);plot3

table1=table(bike$weathersit) #unbalanced, 1>2>3
table(bike$workingday) #unbalanced, 1>0
table(bike$season) #balanced

grid.arrange(plot1, plot2, plot3,nrow=2, ncol=2)

#Continuous variables (temp, hum, windspeed)
p4=ggplot(data=bike,aes(x=temp,y=cnt))+geom_point()+geom_smooth(se=F);p4
p5=ggplot(data=bike,aes(x=hum,y=cnt))+geom_point()+geom_smooth(se=F);p5
p6=ggplot(data=bike,aes(x=windspeed,y=cnt))+geom_point()+geom_smooth(se=F);p6

grid.arrange(p4, p5, p6,nrow=2, ncol=2)

#response variable (cnt)
p7=ggplot(data=bike,aes(x=cnt))+geom_histogram(bins=15,color='black');p7

###Analysis:

#Set up a Poisson regression model that predicts the number of total rental bikes (cnt) 
#in a frequentist manner with the R software used in the course.

#(1) Set up a Poisson regression model that predicts the number of total rental bikes 
#(cnt) in a frequentist manner with the R software used in the course.

poismod1=glm(bike$cnt~.^2,family=poisson(link="log"),data=bike)
summary(poismod1) #32947

poismod1=glm(bike$cnt~.^2,family=poisson(link="log"),data=bike)
summary(poismod1) #32947

poismod12=glm(bike$cnt~.^2,family=poisson(link="identity"),data=bike)
summary(poismod12) #32947

#check for multicollinearity
vif(poismod1) #no vif>30

#(2) Select the most predictive regressors in a classical GLM manner and interpret 
#your results.

n <- nrow(bike)

stepboth1=stepAIC(poismod1,k=2,direction='both') #AIC 32946.89
stepboth2=stepAIC(poismod1,k=log(n),direction='both') #BIC 33110.69

#updated model 1
poismod2=glm(stepboth1,family=poisson(link="log"))
summary(poismod2) 

#check for multicollinearity
vif(poismod2) #no vif>30

#test updated model 1
drop1(poismod2,test = 'Rao') # none
anova(poismod2,test='Rao') # none

drop1(poismod2,test = 'LRT') # none
anova(poismod2,test='LRT') # none

#add quadratic terms
poismod3=glm(bike$cnt~bike$weathersit+bike$workingday+bike$temp+I(bike$temp^2)+
               bike$hum+I(bike$hum^2)+bike$windspeed+I(bike$windspeed^2)+bike$season,family=poisson(link="log"),data=bike)
summary(poismod3) #37837

#check for multicollinearity
vif(poismod3) #no vif>30

stepboth3=stepAIC(poismod3,k=2,direction='both',scope = list(upper=~.^2,lower=~1)) #AIC 22141

#updated model 2
poismod4=glm(stepboth3,family=poisson(link="log"),data=bike)
summary(poismod4) #22141

#check for multicollinearity
vif(poismod4) # this model fits better, but large amounts of multicollinearity

##poismod2 has the best fit##



#(3) Also select the most predictive regressors in an extended GAM manner

###Continuous variables (temp, hum, windspeed)
p4=ggplot(data=bike,aes(x=temp,y=cnt))+geom_point()+geom_smooth(se=F);p4
p5=ggplot(data=bike,aes(x=hum,y=cnt))+geom_point()+geom_smooth(se=F);p5
p6=ggplot(data=bike,aes(x=windspeed,y=cnt))+geom_point()+geom_smooth(se=F);p6

grid.arrange(p4, p5, p6,nrow=2, ncol=2)

apply(bike[, c('temp', 'hum', 'windspeed')],2, function(x) length(unique(x)))

#gam1
library('mgcv')

gam1 <- gam(cnt ~ s(temp, bs='ps', k=20) +
                   s(hum, bs='ps', k=20) +
                   s(windspeed, bs='ps', k=20) +
                   weathersit*workingday +
                   weathersit*season +
                   workingday*season,
                   family='poisson'(link='log'),
                   data=bike,
                   method='REML')

summary(gam1)

#Plotting the Model
par(mfrow=c(1,3)) #to partition the Plotting Window
plot(gam1,residuals=F,cex=1.3,col='red',shade = T,scale = 0) 
gam.check(gam1,col='blue')

#gam2
gam2 <- gam(cnt ~ s(temp, bs='ps', k=40) +
              s(hum, bs='ps', k=40) +
              s(windspeed, bs='ps', k=40) +
              weathersit*workingday +
              weathersit*season +
              workingday*season,
            family='poisson'(link='log'),
            data=bike,
            method='REML')

summary(gam2)

#Plotting the Model
par(mfrow=c(1,3)) #to partition the Plotting Window
plot(gam2,residuals=F,cex=1.3,col='red',shade = T,scale = 0) 
gam.check(gam2,col='blue')

#compare gam1 and gam2
gam1$sp #pentalties
AIC(gam1,gam2) #model 2 has better fit, 34138.87 vs 29694.89

#compare gam2 to poismod2
gam2$sp #pentalties
AIC(gam2,poismod2) #gam2 has better fit, 29694.89 vs 32946.89



#(4) Do the necessary checks to verify that the chosen model(s) fit the data well, 
#e.g. check the link function, the scale of the covariates, etc. In other words, 
#choose the most appropriate procedure. Illustrate your findings with appropriate 
#graphics and illustrate how good prediction.

#check residuals for poismod2
plot(poismod2,residuals=T,cex=1.3,col='red',shade = T,scale = 0) #poor fit

chisq.test(x=bike$cnt,p=poismod2$fitted.values,rescale.p = TRUE) #reject that observed=predicted

chisq.test(x=bike$cnt,p=gam2$fitted.values,rescale.p = TRUE) #reject that observed=predicted

#check fit
#plot poisson and gam
par(mfrow=c(1,1))
hist(bike$cnt, prob=TRUE, col="grey")# prob=TRUE for probabilities not counts
lines(density(poismod2$fitted.values, adjust=4), lty="solid", col="darkgreen", lwd=2) 
lines(density(gam2$fitted.values, adjust=4), lty="solid", col="blue", lwd=2) 
#both models dont fit the data well

install.packages('Metrics')
library('Metrics')
rmse('cnt', poismod2, bike)
rmse('cnt', bike.pois3, bike.dat)

p1 <- predict(poismod2, se.fit=TRUE, type='response')

layout(matrix(c(1, 2), nrow=2, byrow=TRUE))
plot(bike$cnt, fitted(gam2))
lines(fitted(gam2),
      fitted(lm(bike$cnt ~ fitted(gam2))))
plot(bike$cnt, fitted(poismod2))
lines(fitted(poismod2),
      fitted(lm(bike$cnt ~ fitted(poismod2))))

layout(matrix(c(1, 2), nrow=2, byrow=TRUE))
plot(bike$cnt, residuals(gam2))
plot(bike$cnt, residuals(poismod2))

res.dat <- data.frame(res=residuals(gam2), model='gam')
res.dat <- rbind(res.dat,
                 data.frame(res=residuals(poismod2),
                            model='pois'))

boxplot(res ~ model, data=res.dat)



#(5) Fit a negative binomial model and a quasi-Poisson model in a frequentist
#manner if there is overdispersion.

library('AER')
dispersiontest(poismod2,trafo=1) #overdispersion present

dfun <- function(object) {
  with(object,sum((weights * residuals^2)[weights > 0])/df.residual)
}

dfun(poismod2) # dispersion parameter is 85.87 for poisson model

##negative binomial distribution
ngbinmodel1=glm.nb(stepboth1,data = bike)
summary(ngbinmodel1) #AIC 5722.9

#test 1
drop1(ngbinmodel1,test = 'Rao') 
#drop: workingday:temp,workingday:hum,temp:hum,temp:windspeed,hum:windspeed,windspeed:season

anova(ngbinmodel1,test='Rao') 
#drop: workingday,workingday:hum,workingday:windspeed,temp:hum,temp:windspeed,windspeed:season 

#update 1

ngbinmodel2=update(ngbinmodel1,. ~ . -workingday:temp-workingday:hum-temp:hum-temp:windspeed-hum:windspeed-windspeed:season-
                     workingday:hum-workingday:windspeed-temp:hum-temp:windspeed-windspeed:season)
summary(ngbinmodel2) 

#test 2
drop1(ngbinmodel2,test = 'Rao')  
#drop: weathersit:season 

anova(ngbinmodel2,test='Rao') 
#drop: none

#update 2
ngbinmodel3=update(ngbinmodel2,. ~ . - weathersit:season)
summary(ngbinmodel3) 

#test 3
drop1(ngbinmodel3,test = 'Rao')  
#drop weathersit:temp

anova(ngbinmodel3,test='Rao') 
#drop none

#update 3
ngbinmodel4=update(ngbinmodel3,. ~ . - weathersit:temp)
summary(ngbinmodel4) 

#test 4
drop1(ngbinmodel4,test = 'Rao')  
#drop none

anova(ngbinmodel4,test='Rao') 
#drop none

#final model
summary(ngbinmodel4) #Residual deviance:  368.55  on 340  degrees of freedom

plot(ngbinmodel4,residuals=T,cex=1.3,col='red',shade = T,scale = 0) 
#much better fit according to residuals

chisq.test(x=bike$cnt,p=ngbinmodel4$fitted.values,rescale.p = TRUE) #reject that observed=predicted

#outlier removal (69,239,341)
ngbinmodel5=update(ngbinmodel4,subset=c(-69,-239,-341));ngbinmodel5
summary(ngbinmodel5)
AIC(ngbinmodel4,ngbinmodel5) #much lower w/ outliers removed
plot(ngbinmodel5,residuals=T,cex=1.3,col='red',shade = T,scale = 0) 


##quasi-Poisson

qp1 = glm(poismod2,family='quasipoisson',data=bike)
summary(qp1)

drop1(qp1,test = 'Rao') 





##compare all models
AIC(poismod2,gam2,ngbinmodel4) #32946.894, 29694.893, 5716.511

#plot all models
par(mfrow=c(1,1))
hist(bike$cnt, prob=TRUE, col="grey")# prob=TRUE for probabilities not counts
lines(density(poismod2$fitted.values, adjust=0.8), lty="solid", col="darkgreen", lwd=1) 
lines(density(gam2$fitted.values, adjust=0.8), lty="solid", col="blue", lwd=1) 
lines(density(ngbinmodel4$fitted.values, adjust=0.8), lty="solid", col="red", lwd=1) 

