rm(list=ls())

setwd('/home/marc/School/GLM/GLM_project')

library(tidyverse)
library(readxl)
library(mgcv)
library(MASS)
source('./glmP_functions.r')

bike.dat <- as.data.frame(read_excel('./bike_sharing.xlsx'))

imp_cols <- c('weathersit', 'workingday', 'temp',
              'hum', 'windspeed', 'season', 'cnt')
bike.dat <- bike.dat[, imp_cols]
    
str(bike.dat)

bike.dat$weathersit <- factor(bike.dat$weathersit)
bike.dat$workingday <- factor(bike.dat$workingday)
bike.dat$season <- factor(bike.dat$season)

## Question 1
## Second-order model is a better overall fit
bike.pois1 <- glm(cnt ~ .^2, family='poisson', data=bike.dat)
summary(bike.pois1)
bike.pois2 <- glm(cnt ~ ., family='poisson', data=bike.dat)
summary(bike.pois2)

anova(bike.pois2, bike.pois1)
AIC(bike.pois2, bike.pois1)
BIC(bike.pois2, bike.pois1)

## Question 2
## Small but >5 decreases in BIC, negligible increases in AIC
n <- nrow(bike.dat)
bike.pois3 <- step(bike.pois1, direction='both', k=log(n),
                   trace=FALSE)
BIC(bike.pois1, bike.pois3)
AIC(bike.pois1, bike.pois3)

## Question 3
layout(matrix(c(1, 1, 2, 3), 2, 2, byrow=TRUE))
scatter.smooth(bike.dat$temp, bike.dat$cnt, col='grey60')
scatter.smooth(bike.dat$hum, bike.dat$cnt, col='grey60')
scatter.smooth(bike.dat$windspeed, bike.dat$cnt, col='grey60')

apply(bike.dat[, c('temp', 'hum', 'windspeed')],
      2, function(x) length(unique(x)))

bike.gam1 <- gam(cnt ~ s(temp, bs='ps', k=20) +
                     s(hum, bs='ps', k=20) +
                     s(windspeed, bs='ps', k=20) +
                     weathersit*workingday +
                     weathersit*season +
                     workingday*season,
                 family='poisson'(link='log'),
                 data=bike.dat,
                 method='REML')

summary(bike.gam1)
AIC(bike.gam1)

layout(matrix(c(1, 1, 2, 3), 2, 2, byrow=TRUE))
plot(bike.gam1, shade='TRUE')

bike.gam2 <- gam(cnt ~ s(temp, bs='ps', k=20) +
                     s(hum, bs='ps', k=20) +
                     s(windspeed, bs='ps', k=20) +
                     weathersit*workingday +
                     weathersit*season +
                     workingday*season,
                 family='poisson'(link='log'),
                 data=bike.dat,
                 method='REML', select=TRUE)

summary(bike.gam2)
AIC(bike.gam2)

layout(matrix(c(1, 1, 2, 3), 2, 2, byrow=TRUE))
plot(bike.gam2, shade='TRUE', residuals=FALSE, scale=0)

layout(matrix(c(1, 1, 2, 3), 2, 2, byrow=TRUE))
plot(bike.gam2, shade='TRUE', residuals=TRUE)

gam.check(bike.gam2, type='deviance')

bike.gam2$sp

## Question 4
## GAM as lower RMSE by quite a lot
rmse('cnt', bike.gam2, bike.dat)
rmse('cnt', bike.pois3, bike.dat)

p1 <- predict(bike.gam2, se.fit=TRUE, type='response')

layout(matrix(c(1, 2), nrow=2, byrow=TRUE))
plot(bike.dat$cnt, fitted(bike.gam2))
lines(fitted(bike.gam2),
      fitted(lm(bike.dat$cnt ~ fitted(bike.gam2))))
plot(bike.dat$cnt, fitted(bike.pois3))
lines(fitted(bike.pois3),
      fitted(lm(bike.dat$cnt ~ fitted(bike.pois3))))

layout(matrix(c(1, 2), nrow=2, byrow=TRUE))
plot(bike.dat$cnt, residuals(bike.gam2))
plot(bike.dat$cnt, residuals(bike.pois3))

res.dat <- data.frame(res=residuals(bike.gam2), model='gam')
res.dat <- rbind(res.dat,
                 data.frame(res=residuals(bike.pois3),
                            model='pois'))
boxplot(res ~ model, data=res.dat)

## Question 5
bike.nb1 <- glm.nb(formula(bike.pois3), data=bike.dat)
bike.qpois1 <- glm(formula(bike.pois3),
                   family='quasipoisson', data=bike.dat)

AIC(bike.nb1, bike.pois3)
AIC(bike.qpois1, bike.pois3)

summary(bike.qpois1)
summary(bike.nb1)




