rm(list=ls())

setwd('/home/marc/School/GLM/glmproject')

library(tidyverse)
library(readxl)
library(mgcv)
library(MASS)
library(lattice)
library(splines)
library(corrplot)
library(RColorBrewer)
library(scales)
source('./glmP_functions.r')

imdb.dat <- as.data.frame(read_excel('./IMDb.xlsx'))
keep.cols <- c('budget', 'profit', 'director_facebook_likes',
               'content_rating')
imdb.dat <- imdb.dat[, keep.cols]
str(imdb.dat)

imdb.dat$content_rating <- factor(imdb.dat$content_rating)

## Question 3
lm(profit ~ poly(director_facebook_likes, degree=3),
   data=imdb.dat)

## Question 4
imdb.dat$dprofit <- as.numeric(imdb.dat$profit > 0)

summary(imdb.dat$dprofit)

layout(matrix(c(1, 2), ncol=2))
plot(imdb.dat$budget, imdb.dat$dprofit)
lines(lowess(imdb.dat$budget, imdb.dat$dprofit))
plot(imdb.dat$director_facebook_likes, imdb.dat$dprofit)
lines(lowess(imdb.dat$director_facebook_likes, imdb.dat$dprofit))

tapply(imdb.dat$dprofit, imdb.dat$content_rating, function(x) {
    sum(x)/length(x)
})


