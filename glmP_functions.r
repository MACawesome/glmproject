library(stringr)

Mode <- function(x) {
    ## Compute the mode, thanks Ken Williams (stack overflow)
  ux <- unique(x)
  ux[which.max(tabulate(match(x, ux)))]
}

aic_comp <- function(model) {
    n <- length(fitted(model))
    sig2 <- summary(model)$sigma^2
    df <- summary(model)$df[1]
    aic <- n*log(sig2) + 2*(df)
    return(aic)    
}

loocv <- function(model) {
    hii <- lm.influence(model)$h
    out<- mean(((model[['residuals']])/(1-hii))^2)
    return(out)
}

rmse <- function(y, model, data) {
    yhat <- predict(model, type='response')
    y <- data[[y]]
    rmse <- sum((yhat - y)^2)/length(y)
    rmse <- sqrt(rmse)
    return(rmse)
}

trunc_pol <- function(y, x, data, deg=2, k=2) {
    ## Returns a list with a lm object
    x <- data[[x]]
    y <- data[[y]]
    stops <- seq(0, 1, length.out=2+k)
    k <- quantile(x, stops)
    k <- k[-1]
    k <- k[-length(k)]
    b_g <- sapply(1:deg, function(i) {
        return(x^i)
    })
    b_l <- sapply(k, function(i) {
        (x-i)^deg * (x > i)
    })
    fit <- lm(y ~ b_g + b_l)
    loo_cv <- loocv(fit)
    aic <- aic_comp(fit)
    adj_rs <- summary(fit)$adj.r.squared
    out <- list(fit=fit, loo_cv=loo_cv,
                aic=aic, adj_rs=adj_rs)
    return(out)
}

b_spliner <- function(y, x, data, deg=2, k=3) {
    ## y and x as character
    ## k are the inner knots
    ## compute a bspline with fitted values
    x <- data[[x]]
    y <- data[[y]]
    minmax_x <- range(x)
    steps <- diff(minmax_x)/(k-1)
    knots <- seq(minmax_x[1] - deg * steps,
                 minmax_x[2] + deg * steps,
                 by=steps)
    B <- spline.des(knots, x, ord=(deg+1))$design
    beta <- solve(t(B)%*%B) %*% t(B) %*% y
    fitted <- B %*% beta
    ## Computing aic, adj_rs and gcv
    n <- length(x)
    S <- B%*%solve(t(B)%*%B)%*%t(B)
    diags <- diag(S)
    trs <- mean(diags)
    df <- sum(diags)
    fit <- S %*% y
    sigma2 <- sum((y-fit)^2)/n
    ## Somehow the rs is no good
    rs <- 1 - sigma2 * n/sum((y-mean(y))^2)
    aic <- n*log(sigma2) + 2*(df+1)
    adj_rs <- 1 - (1 - rs)*((n-1)/(n-df))
    gcv <- mean(((y - fit)/(1-trs))^2)
    
    out <- list(B=B,
                beta=beta,
                fitted=fitted,
                aic=aic,
                adj_rs=adj_rs,
                gcv=gcv)
    return(out)
}

p_spliner <- function(y, x, data, deg=3, k=5) {
        ## y and x as character
    ## k are the inner knots
    ## compute a bspline with fitted values
    x <- data[[x]]
    y <- data[[y]]
    minmax_x <- range(x)
    steps <- diff(minmax_x)/(k-1)
    knots <- seq(minmax_x[1] - deg * steps,
                 minmax_x[2] + deg * steps,
                 by=steps)

    D2 <- matrix(0, k, k+2)
    for(i in 1:k) {
        D2[i, i] <- 1
        D2[i, i+1] <- -2
        D2[i, i+2] <- 1
    }
    K2 <- t(D2) %*% D2
    
    B <- spline.des(knots, x, ord=(deg+1))$design

    c <- 1e3
    lambda <- seq(0.0005, 0.05, length.out=c)
    gcv <- rep(0, c)
    aic <- rep(0, c)
    adj_rs <- rep(0, c)
    n <- length(x)
    
    for(i in 1:c) {
        S <- B%*%solve(t(B)%*%B + lambda[i]*K2)%*%t(B)
        diags <- diag(S)
        trs <- mean(diags)
        df <- sum(diags)
        fit <- c(S%*%y)
        gcv[i] <- mean(((y - fit)/(1-trs))^2)
        sigma2 <- sum((y-fit)^2)/n
        ## somehow the rs is not good can't trust it
        rs <- 1 - sigma2 * n/sum((y-mean(y))^2)
        adj_rs[i] <- 1 - (1 - rs)*((n-1)/(n-length(beta)))
        aic[i] <- n*log(sigma2) + 2*(df+1)
    }
    
    lambda_min <- lambda[which.min(gcv)]
    gcv_min <- gcv[which.min(gcv)]
    aic_min <- aic[which.min(gcv)]
    adj_rs_min <- adj_rs[which.min(gcv)]
    
    beta <- solve(t(B)%*%B + lambda_min*K2) %*% t(B) %*% y
    fitted <- B %*% beta
    out <- list(B=B,
                beta=beta,
                fitted=fitted,
                lambda_min=lambda_min,
                gcv_min=gcv_min,
                aic_min=aic_min,
                adj_rs_min=adj_rs_min)
    return(out)
}

test <- formula(thing ~ thing2 + thing3)

gam_3_finder <- function(mink=5, maxk=20) {
    k1 <- seq(mink, maxk)
    k2 <- seq(mink, maxk)
    smooth.terms1 <- c("tp", "ps", "cp", "bs")
    smooth.terms2 <- c("tp", "ps", "cp", "bs")
    hp_grid <- expand.grid(k1, k2,
                           smooth.terms1, smooth.terms2)
    
    print(nrow(hp_grid))
    return(NULL)
    l_grid <- nrow(hp_grid)
    gcv_vec <- rep(NA, l_grid)
    for(i in 1:l_grid) {
        if(i %% 50 == 0) {
            print(paste0(i/l_grid, ' completed'))
        }
        tryCatch({
            tmp.fit <- gam(profit ~ s(budget, k=hp_grid[i, 1],
                                      bs=hp_grid[i, 3]) +
                               s(director_facebook_likes,
                                 k=hp_grid[i, 2],
                                 bs=hp_grid[i, 4]), data=imdb.dat)
            gcv_vec[i] <- tmp.fit$gcv.ubre
        },
        error=function(e) {
            NULL
        },
        warning=function(w) NULL)
    }
    best_index <- which.min(gcv_vec)
    best_params <- hp_grid[best_index, ]
    best_gcv <- gcv_vec[best_index]
    
    out <- list(best_gcv=best_gcv,
                best_params=best_params)
    return(out)
}

gam_4_finder_ct <- function(mink=5, maxk=20) {
    k1 <- seq(mink, maxk)
    k2 <- seq(mink, maxk)
    smooth.terms1 <- c("tp", "ps", "cp", "bs")
    smooth.terms2 <- c("tp", "ps", "cp", "bs")
    links=c('logit', 'probit')
    hp_grid <- expand.grid(k1, k2,
                           smooth.terms1, smooth.terms2,
                           links)
    print(hp_grid)
    l_grid <- nrow(hp_grid)
    gcv_vec <- rep(NA, l_grid)
    for(i in 1:l_grid) {
        if(i %% 50 == 0) {
            print(paste0(i/l_grid, ' completed'))
        }
        tryCatch({
            tmp.fit <- gam(dprofit ~ s(budget, k=hp_grid[i, 1],
                                      bs=hp_grid[i, 3]) +
                               s(director_facebook_likes,
                                 k=hp_grid[i, 2],
                                 bs=hp_grid[i, 4]) +
                          content_rating,
                           family=binomial(hp_grid[i, 5]),
                           data=imdb.dat)
            gcv_vec[i] <- tmp.fit$gcv.ubre
        },
        error=function(e) {
            print('non convergence')
        },
        warning=function(w) print('can\'t get no satisfaction'))
    }
    best_index <- which.min(gcv_vec)
    best_params <- hp_grid[best_index, ]
    best_gcv <- gcv_vec[best_index]
    
    out <- list(best_gcv=best_gcv,
                best_params=best_params)
    return(out)
}

gam_4_finder_noct <- function(mink=5, maxk=20) {
    k1 <- seq(mink, maxk)
    k2 <- seq(mink, maxk)
    smooth.terms1 <- c("tp", "ps", "cp", "bs")
    smooth.terms2 <- c("tp", "ps", "cp", "bs")
    links=c('logit', 'probit', 'cloglog')
    hp_grid <- expand.grid(k1, k2,
                           smooth.terms1, smooth.terms2,
                           links)
    print(hp_grid)
    l_grid <- nrow(hp_grid)
    gcv_vec <- rep(NA, l_grid)
    for(i in 1:l_grid) {
        if(i %% 50 == 0) {
            print(paste0(i/l_grid, ' completed'))
        }
        tryCatch({
            tmp.fit <- gam(I(profit > 0) ~ s(budget, k=hp_grid[i, 1],
                                      bs=hp_grid[i, 3]) +
                               s(director_facebook_likes,
                                 k=hp_grid[i, 2],
                                 bs=hp_grid[i, 4]),
                           family=binomial(as.character(hp_grid[i, 5])),
                           data=imdb.dat)
            gcv_vec[i] <- tmp.fit$gcv.ubre
        },
        error=function(e) {
            NULL
        },
        warning=function(w) NULL)
    }
    best_index <- which.min(gcv_vec)
    best_params <- hp_grid[best_index, ]
    best_gcv <- gcv_vec[best_index]
    
    out <- list(best_gcv=best_gcv,
                best_params=best_params)
    return(out)
}

gam_4_finder_cut <- function(mink=0, maxk=100) {
  k <- seq(mink,maxk)
  l_grid <- length(k)
  gcv_vec <- rep(NA, l_grid)
  for (i in 1:l_grid) {
    imdb.dat$temprofit <- as.factor(imdb.dat$profit>k[i])
    if(i %% 50 == 0) {
      print(paste0(i/l_grid, ' completed'))
    }
    tryCatch({tmp.fit <- gam(imdb.dat$temprofit ~ s(budget, bs='cp', k=6) +
                              s(director_facebook_likes, bs='cp', k=10),
                            family=binomial("logit"), data=imdb.dat)
    gcv_vec[i] <- tmp.fit$gcv.ubre
    },
    error=function(e) {
      NULL
    },
    warning=function(w) NULL)
  }
  best_index <- which.min(gcv_vec)
  best_gcv <- gcv_vec[best_index]
  best_cutoff <- k[best_index]
  out <- list(best_gcv=best_gcv,
              best_cutoff=best_cutoff)
  return(out)
}
