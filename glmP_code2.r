rm(list=ls())

setwd('/home/marc/School/GLM/glmproject')

library(tidyverse)
library(readxl)
library(mgcv)
library(MASS)
library(lattice)
library(splines)
library(corrplot)
library(RColorBrewer)
library(scales)
library(gridExtra)
source('./glmP_functions.r')

imdb.dat <- as.data.frame(read_excel('./IMDb.xlsx'))
keep.cols <- c('budget', 'profit', 'director_facebook_likes',
               'content_rating')
imdb.dat <- imdb.dat[, keep.cols]
str(imdb.dat)

imdb.dat$content_rating <- factor(imdb.dat$content_rating)

## Some basic description
apply(imdb.dat[, -4], 2, function(x) {
    round(c(r=range(x), m=mean(x), s=sd(x)), 2)
})


M <- cor(imdb.dat[, -4])
M[upper.tri(M, diag=TRUE)]

run_plots <- FALSE
## Question 1
## Bivariate
if(run_plots) {
    jpeg('./figures/smoothers_desc_p2.jpg', width=8, height=6,
         units='in', res=300)
    layout(matrix(c(1, 2, 3, 3), 2, 2, byrow=TRUE))
    boxplot(profit ~ content_rating, data=imdb.dat,
            ylab='Profit', xlab='Rating')
    scatter.smooth(imdb.dat$budget, imdb.dat$profit, ylab='',
                   xlab='Budget')
    scatter.smooth(imdb.dat$director_facebook_likes, imdb.dat$profit,
                   xlab='Director likes', ylab='Profit')
    dev.off()
}

## Interactions with content_rating
if(run_plots) {
    panel.smoother <- function(x, y) {
        panel.xyplot(x, y) # show points
        panel.loess(x, y)  # show smoothed line
    }
    xyplot(profit ~ budget | content_rating, data=imdb.dat,
           panel=panel.smoother)

    xyplot(profit ~ director_facebook_likes | content_rating, data=imdb.dat,
           panel=panel.smoother)
}

## Question 2
## Polynomial regression model
imdb.lm1 <- lm(profit ~ budget, data=imdb.dat)
imdb.lm2 <- lm(profit ~ budget + I(budget^2), data=imdb.dat)
imdb.lm3 <- lm(profit ~ budget + I(budget^2) +
                   I(budget^3), data=imdb.dat)

imdb.lm1 <- lm(profit ~ poly(budget, 1), data=imdb.dat)
imdb.lm2 <- lm(profit ~ poly(budget, 2), data=imdb.dat)
imdb.lm3 <- lm(profit ~ poly(budget, 3), data=imdb.dat)

imdb.lm_list <- list(lm1=imdb.lm1,
                     lm2=imdb.lm2,
                     lm3=imdb.lm3)


## Can't use AIC anymore, must use aic_comp for comparability
sapply(imdb.lm_list, loocv)
sapply(imdb.lm_list, function(x) summary(x)$adj.r.squared)
sapply(imdb.lm_list, aic_comp)
sapply(imdb.lm_list, AIC)

if(run_plots) {
    plot(imdb.dat$budget, imdb.dat$profit, type='p', col=alpha('black', 0.4))
    ord <- order(imdb.dat$budget)
    lines(imdb.dat$budget[ord], fitted(imdb.lm1)[ord])
    lines(imdb.dat$budget[ord], fitted(imdb.lm2)[ord], lty=2)
    lines(imdb.dat$budget[ord], fitted(imdb.lm3)[ord], lty=3)
}

## Truncated polynomial splines of degree 2(k=2, 3 and 5)
imdb.tp1 <- trunc_pol('profit', 'budget', imdb.dat, k=2)
imdb.tp2 <- trunc_pol('profit', 'budget', imdb.dat, k=3)
imdb.tp3 <- trunc_pol('profit', 'budget', imdb.dat, k=5)

imdb.tp_list <- list(tp1=imdb.tp1,
                     tp2=imdb.tp2,
                     tp3=imdb.tp3)

sapply(imdb.tp_list, function(x) {
    round(c(loo_cv=x[['loo_cv']],
      aic=x[['aic']],
      adj_rs=x[['adj_rs']]), 3)
})

if(run_plots) {
    layout(matrix(c(1, 1, 2, 3), nrow=2, byrow=TRUE))

    plot(imdb.dat$budget, imdb.dat$profit, type='p', col=alpha('black', 0.4))
    ord <- order(imdb.dat$budget)
    lines(imdb.dat$budget[ord], fitted(imdb.tp1$fit)[ord])
    lines(imdb.dat$budget[ord], fitted(imdb.tp2$fit)[ord], lty=2)
    lines(imdb.dat$budget[ord], fitted(imdb.tp3$fit)[ord], lty=3)
}

## B-spline of degree 2 (k=3, 5 and 8 knots)
imdb.bsp1 <- b_spliner('profit', 'budget', deg=2, k=3, imdb.dat)
imdb.bsp2 <- b_spliner('profit', 'budget', deg=2, k=5, imdb.dat)
imdb.bsp3 <- b_spliner('profit', 'budget', deg=2, k=8, imdb.dat)

imdb.bsp1_lm <- lm(profit ~ bs(budget, knots=3, degree=2) - 1, data=imdb.dat)


imdb.bsp_list <- list(bsp1=imdb.bsp1,
                      bsp2=imdb.bsp2,
                      bsp3=imdb.bsp3)

sapply(imdb.bsp_list, function(x) {
    round(c(loo_cv=x[['gcv']],
      aic=x[['aic']],
      adj_rs=x[['adj_rs']]), 3)
})

run_plots <- TRUE
ord <- order(imdb.dat$budget)
if(run_plots) {
    plot(imdb.dat$budget, imdb.dat$profit, type='p')
    lines(imdb.dat$budget[ord], imdb.bsp1$fitted[ord])
    lines(imdb.dat$budget[ord], imdb.bsp2$fitted[ord], lty=2)
    lines(imdb.dat$budget[ord], imdb.bsp3$fitted[ord], lty=3)
}

## Imitating plots in overleaf for the b-spline


## Cubic P-splines (consider k=5, 8 and 20 knots)
imdb.psp1 <- p_spliner('profit', 'budget', k=5, data=imdb.dat)
imdb.psp2 <- p_spliner('profit', 'budget', k=8, data=imdb.dat)
imdb.psp3 <- p_spliner('profit', 'budget', k=20, data=imdb.dat)
str(imdb.psp1)
str(imdb.psp2)
str(imdb.psp3)

imdb.psp_list <- list(psp1=imdb.psp1,
                      psp2=imdb.psp2,
                      psp3=imdb.psp3)

sapply(imdb.psp_list, function(x) {
    round(c(loo_cv=x[['gcv_min']],
      aic=x[['aic_min']],
      adj_rs=x[['adj_rs_min']],
      lambda_min=x[['lambda_min']]), 3)
})

ord <- order(imdb.dat$budget)

if(run_plots) {
    plot(imdb.dat$budget, imdb.dat$profit, type='p')
    lines(imdb.dat$budget[ord], imdb.psp1$fitted[ord])
    lines(imdb.dat$budget[ord], imdb.psp2$fitted[ord],
          lty=2)
    lines(imdb.dat$budget[ord], imdb.psp3$fitted[ord],
          lty=3)
    legend('topleft', lty=c(1, 2, 3),
           legend=c('5 knots', '8 knots', '20 knots'))
}

## Imitate plot b-spline and p-spline in figure
p <- ggplot(data=imdb.dat, aes(x=budget, y=profit)) +
    geom_point(alpha=0.2) + theme_bw() +
    labs(colour="", x="Budget", y="Profit")

pbsp <- p +  geom_line(aes(x=imdb.dat$budget[ord],
                         y=imdb.bsp1$fitted[ord],
                         colour='3 knots')) +
    geom_line(aes(x=imdb.dat$budget[ord],
                  y=imdb.bsp2$fitted[ord],
                  colour='5 knots')) +
    geom_line(aes(x=imdb.dat$budget[ord],
                  y=imdb.bsp3$fitted[ord],
                  colour='8 knots')) +
    ggtitle('B-splines')

ppsp <- p +  geom_line(aes(x=imdb.dat$budget[ord],
                         y=imdb.psp1$fitted[ord],
                         colour='5 knots')) +
    geom_line(aes(x=imdb.dat$budget[ord],
                  y=imdb.psp2$fitted[ord],
                  colour='8 knots')) +
    geom_line(aes(x=imdb.dat$budget[ord],
                  y=imdb.psp3$fitted[ord],
                  colour='20 knots')) +
    ggtitle('P-splines')



if(run_plots) {
    jpeg('./figures/bsplines_q2.jpg', width=12, height=5,
         units='in', res=300)
    grid.arrange(pbsp, ppsp, ncol=2)
    dev.off()
}


## Making 2*2 figure of the 4 modeling approaches
if(run_plots) {
    jpeg('./figures/splines_plots_p2.jpg', width=8, height=6,
         units='in', res=300)
    layout(matrix(c(1, 2, 3, 4), nrow=2))
    ## P1
    plot(imdb.dat$budget, imdb.dat$profit, type='p',
         col=alpha('black', 0.4), main='Polynomial regressions',
         ylab='Profit', xlab='Budget')
    ord <- order(imdb.dat$budget)
    lines(imdb.dat$budget[ord], fitted(imdb.lm1)[ord], col='red')
    lines(imdb.dat$budget[ord], fitted(imdb.lm2)[ord], lty=2)
    lines(imdb.dat$budget[ord], fitted(imdb.lm3)[ord], lty=3)
    legend('topleft', lty=c(1, 2, 3), col=c('red', 'black', 'black'),
           legend=c('1st degree', '2nd degree', '3rd degree'))
    ## P2
    plot(imdb.dat$budget, imdb.dat$profit, type='p',
         col=alpha('black', 0.4), main='Truncated polynomials',
         ylab='Profit', xlab='Budget')
    ord <- order(imdb.dat$budget)
    lines(imdb.dat$budget[ord], fitted(imdb.tp1$fit)[ord], col='red')
    lines(imdb.dat$budget[ord], fitted(imdb.tp2$fit)[ord], lty=2)
    lines(imdb.dat$budget[ord], fitted(imdb.tp3$fit)[ord], lty=3)
    legend('topleft', lty=c(1, 2, 3), col=c('red', 'black', 'black'),
           legend=c('2 knots', '3 knots', '5 knots'))
    ## P3
    plot(imdb.dat$budget, imdb.dat$profit, type='p',
         col=alpha('black', 0.4), main='B-splines',
         ylab='', xlab='Budget')
    lines(imdb.dat$budget[ord], imdb.bsp1$fitted[ord], col='red')
    lines(imdb.dat$budget[ord], imdb.bsp2$fitted[ord], lty=2)
    lines(imdb.dat$budget[ord], imdb.bsp3$fitted[ord], lty=3)
    legend('topleft', lty=c(1, 2, 3), col=c('red', 'black', 'black'),
           legend=c('3 knots', '5 knots', '8 knots'))
    ## P4
    plot(imdb.dat$budget, imdb.dat$profit, type='p',
         col=alpha('black', 0.4), main='P-splines',
         ylab='', xlab='Budget')
    lines(imdb.dat$budget[ord], imdb.psp1$fitted[ord])
    lines(imdb.dat$budget[ord], imdb.psp2$fitted[ord],
          lty=2)
    lines(imdb.dat$budget[ord], imdb.psp3$fitted[ord],
          lty=3, col='red')
    legend('topleft', lty=c(1, 2, 3), col=c('black', 'black', 'red'),
           legend=c('5 knots', '8 knots', '20 knots'))
    ## Delete window
    dev.off()
}

## Question 3
imdb.gam1 <- gam(profit ~ s(budget, bs='ps', k=8) +
                     s(director_facebook_likes, bs='ps', k=8) +
                     content_rating,
                 data=imdb.dat)

imdb.lm4 <- lm(profit ~ budget*content_rating +
                   director_facebook_likes*content_rating,
               data=imdb.dat)

loocv(imdb.lm4)
imdb.gam1$gcv.ubre

summary(imdb.lm4)
summary(imdb.gam1)

AIC(imdb.lm4)
AIC(imdb.gam1)

## Best spec finder for question 3 (takes about 3 minutes to run)
bestfind1 <- TRUE
if(bestfind1) {
    q3_bestspec <- gam_3_finder(mink=5, maxk=20)
}

imdb.gam1 <- gam(profit ~ s(budget, bs='cp', k=16) +
                     s(director_facebook_likes, bs='ps', k=13),
                 data=imdb.dat)

summary(imdb.gam1)

AIC(imdb.gam1)


## Question 4
imdb.dat$dprofit <- as.factor(imdb.dat$profit > 0)

logit.gam1 <- gam(dprofit ~ s(budget, bs='cp', k=16) +
                    s(director_facebook_likes, bs='ps', k=13),
                 family=binomial("logit"), data=imdb.dat)

summary(logit.gam1)
loocv(logit.gam1)
AIC(logit.gam1)

bestfind2 <- TRUE
if(bestfind2) {
  q4_bestspec2 <- gam_4_finder_noct(mink=5, maxk=15)
}

q4_bestspec2

logit.gam <- gam(dprofit ~ s(budget, bs='cp', k=6) +
                   s(director_facebook_likes, bs='cp', k=10),
                 family=binomial("logit"), data=imdb.dat)

summary(logit.gam)

logit.glm <- glm(dprofit ~ budget + director_facebook_likes + 
                content_rating, family = binomial("logit"), data = imdb.dat)
summary(logit.glm)


AIC(logit.gam)
AIC(logit.glm)

par(mfrow=c(2,3))
hist(budget,col="blue")
hist(director_facebook_likes,col="blue")
plot(profit,col="blue")

hist(imdb.dat$budget,col="blue")
hist(imdb.dat$director_facebook_likes,col="blue")
plot(imdb.dat$profit,col="blue")

# Try deleting extrem observations

imdb.dat <- imdb.dat[which(imdb.dat$profit<170),]
imdb.dat <- imdb.dat[which(imdb.dat$director_facebook_likes<1000),]

bestfind22 <- TRUE
if(bestfind22) {
  q4_bestspec22 <- gam_4_finder_noct(mink=5, maxk=15)
}

q4_bestspec22

logit.gam_d <- gam(imdb.dat$dprofit ~ s(imdb.dat$budget, bs='cp', k=14) +
                   s(imdb.dat$director_facebook_likes, bs='tp', k=5),
                 family=binomial("probit"), data=imdb.dat)

summary(logit.gam_d)

#r.dev <- residuals(logit.gam, type = "deviance")
#plot(director_facebook_likes,r.dev,xlab="director_facebook_likes (years)",ylab="Deviance residual",
#     cex.lab=1.5,cex.axis=1.3)
#loess.dev <- loess(r.dev~director_facebook_likes)
#lo.pred <- predict(loess.dev, se=T)
#orderdirector_facebook_likes <- order(director_facebook_likes)
#lines(director_facebook_likes[orderdirector_facebook_likes],lo.pred$fit[orderdirector_facebook_likes],col="blue",lwd=3)
#lines(director_facebook_likes[orderdirector_facebook_likes],lo.pred$fit[orderdirector_facebook_likes]+2*lo.pred$s[orderdirector_facebook_likes], lty=2,col="red")
#lines(director_facebook_likes[orderdirector_facebook_likes],lo.pred$fit[orderdirector_facebook_likes]-2*lo.pred$s[orderdirector_facebook_likes], lty=2,col="red")

# influencial observatons
N <- length(imdb.dat$profit); id <- 1:N
# Global influence plots
cook.bw <- cooks.distance(logit.gam_d)
plot(id,cook.bw,type="l",xlab="Identification",
     ylab="Cook's distance",
     cex.lab=1.5,cex.axis=1.3)

# Try changing the criterion of success instead of 0

bestfind3 <- TRUE
if(bestfind3) {
  q4_bestcutoff <- gam_4_finder_cut(mink=0, maxk=100)
}

q4_bestcutoff

logit.gam2 <- gam(I(imdb.dat$profit>98) ~ s(imdb.dat$budget, bs='cp', k=14) +
                   s(imdb.dat$director_facebook_likes, bs='tp', k=5),
                 family=binomial("probit"), data=imdb.dat)

summary(logit.gam2)
AIC(logit.gam2)

par(mfrow=c(2,3))
plot(logit.gam_d,residuals=FALSE,cex=1.3,col="red",shade=TRUE,scale=0)
gam.check(logit.gam_d,col="blue")

logit.gam3 <- gam(I(imdb.dat$profit>0) ~ s(imdb.dat$budget, bs='cp', k=14) +
                    s(imdb.dat$director_facebook_likes, bs='tp', k=5) + content_rating,
                  family=binomial("probit"), data=imdb.dat)
summary(logit.gam3)
AIC(logit.gam_d,logit.gam3)

