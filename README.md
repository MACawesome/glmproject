# glmProject

## Division of labour ##

### Part 1 ###
* Hajime Alabanza
* Qiming Sun

### Part 2 ###
* Marc-André Chénier (Question 1 and 2)
* Qiaoyubing Sun (Question 3)
* Runyang Wang (Question 4)

#### Model recommendations Q3 and Q4 ####
These recommendations are obtained using the CV functions in the
functions r file. The models only include budget and facebook likes as
covariates. Models with content-rating just appear to fit less well
overall and it's difficult to choose covariates with GAM.

In binomial gam model: 3 link functions are tried: logit, probit and
cloglog. The tried basic spline functions are cp, ps, tp, bs. 

For Q3, the recommended gaussian model has: 
  * GCV of 2852.403 
  * 16 knots for budget spline function
  * 13 knots for facebook like spline function
  * Cubic (penalized) spline for budget
  * Quadratic penalized spline for facebook likes

For Q4, the recommend gaussian model has: 
  * GCV of 0.392
  * 6 knots for budget
  * 10 knots for facebook likes
  * Cubic (penalized) spline for budget
  * Cubic (penalized) spline for facebook likes
  * Logit link function

## Meeting on Tuesday ##
For the tuesday meeting, we should still settle a time. The team is
welcome to my appartment. Table is large. Adress is 161
Mechelsestraat, app. 203 in Leuven. 

## Bug notification ##
* Revise equation 3 and 6 (M-A), think definition of 3 is wrong, gotta check why penalization term start at j=3 for 6
* there should have some improvement in Question 2's plot
## Plan for today (M-A, sunday 20-05) ##
Finish question 2


